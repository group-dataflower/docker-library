# https://gitlab.com/group-dataflower/docker-library/python-mapnik/

## Maintained by: [DATAFLOWER](https://gitlab.com/group-dataflower)

This is an unofficial Docker image for [python-mapnik](https://github.com/mapnik/python-mapnik).

Thank you to the [mapnik](https://github.com/mapnik) project.
