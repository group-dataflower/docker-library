# Copyright (C) 2021 Eric Man <eric@dataflower.com.au>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

FROM python:3.9.1
MAINTAINER Eric Man <eric@dataflower.com.au>

ARG MAPNIK_VERSION=v3.0.24
ARG PYTHON_MAPNIK_VERSION=7da019cf9eb12af8f8aa88b7d75789dfcd1e901b

# Download Mapnik source code.
RUN cd /opt && \
    wget https://github.com/mapnik/mapnik/releases/download/$MAPNIK_VERSION/mapnik-$MAPNIK_VERSION.tar.bz2 && \
    tar xjf mapnik-$MAPNIK_VERSION.tar.bz2

# Install Mapnik dependencies
RUN apt-get update -y
RUN apt-get install -y \
    libboost-dev \
    libboost-filesystem-dev \
    libboost-program-options-dev \
    libboost-python-dev \
    libboost-regex-dev \
    libboost-system-dev \
    libboost-thread-dev \
    libicu-dev \
    libtiff5-dev \
    libfreetype6-dev \
    libpng-dev \
    libxml2-dev \
    libproj-dev \
    libsqlite3-dev \
    libgdal-dev \
    libcairo-dev \
    python-cairo-dev \
    postgresql-contrib \
    libfreetype6-dev \
    libharfbuzz-dev

# Mapnik requires python2.7
RUN cd /opt/mapnik-$MAPNIK_VERSION && \
    PYTHON=python2.7 ./configure && \
    make PYTHON=python2.7 && \
    make PYTHON=python2.7 install

# Download Mapnik Python bindings source code.
RUN cd /opt && \
    wget https://github.com/mapnik/python-mapnik/archive/$PYTHON_MAPNIK_VERSION.zip && \
    unzip $PYTHON_MAPNIK_VERSION.zip && \
    mv python-mapnik-* python-mapnik

# Install PyCairo
RUN pip3.9 install pycairo

# Build and install Mapnik Python bindings.
RUN cd /opt/python-mapnik && \
    export CPPFLAGS='-I/opt/mapnik-$MAPNIK_VERSION/include -I/usr/local/lib/python3.9/site-packages/cairo/include' && \
    PYCAIRO=true python3.9 setup.py install

CMD ["python3"]
