# https://gitlab.com/group-dataflower/docker-library/osm2psql/

## Maintained by: [DATAFLOWER](https://gitlab.com/group-dataflower)

This is an unofficial Docker image for [osm2psql](https://github.com/openstreetmap/osm2pgsql).
